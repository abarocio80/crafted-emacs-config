;;; config-erc.el --- Configuración de erc           -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Alejandro Barocio A.

;; Author: Alejandro Barocio A. <abarocio80@gmail.com>
;; Keywords: comm

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(require 'config-tab-bar)
(defun aba/erc-tab ()
  (when (and (fboundp 'aba/exwm-monitor-get)
             (string-equal "exwm" (getenv "WM")))
    (exwm-workspace-switch-create 4))
  (aba/tab-bar-switch-to-tab-ensure-unique "IRC:erc")
  (tab-bar-change-tab-group "Comm"))

(setq erc-server                  nil
      erc-port                    nil
      erc-nick                    nil
      erc-user-full-name          nil
      erc-password                nil
      erc-prompt-for-password     t
      erc-track-shorten-start     8
      erc-autojoin-channels-alist '(("libera.chat"
                                     "#emacs"
                                     "#guix"
                                     "#systemcrafters"))
      erc-kill-buffer-on-part     t
      erc-auto-query              'bury
      erc-prompt (lambda () (format "%s>" (buffer-name))))


(setq erc-fill-column        78
      erc-fill-function      'erc-fill-static
      erc-hide-list          '("JOIN" "PART" "QUIT")
      erc-fill-static-center 20)

;; Creamos un comando que extraiga la información sensible de su lugar
;; de almacenamiento seguro.
(defun aba/erc ()
  "Connect to irc."
  (interactive)
  (aba/erc-tab)
  (erc-tls :server    (or (password-store-get-field "irc/libera.chat" "server")
                          erc-server)
           :port      (or (password-store-get-field "irc/libera.chat" "port")
                          erc-port)
           :nick      (or (password-store-get-field "irc/libera.chat" "nick")
                          erc-nick)
           :full-name (or (password-store-get-field "irc/libera.chat" "user")
                          erc-user-full-name)
           :password  (password-store-get           "irc/libera.chat")
           ))

;; Establecemos una combinación de teclas para ejecutar el comando
;; anterior.
;; (define-key leader-map (kbd "i") #'aba/erc)

(provide 'config-erc)
;;; config-erc.el ends here
