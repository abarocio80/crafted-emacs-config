;;; config-screencast.el --- Configuracion para screencast  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Alex Barocio

;; Author: Alex Barocio <alex@ithilien>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(require 'crafted-screencast)

(defvar aba/keycast-mode '(:modeline nil :tab-bar nil)
  "Wether to activate keycast or not.")

(defun aba/toggle-keycast--tab-bar (&optional toggle)
  "Toggles keycast on/off on the tab-bar."
  (keycast-tab-bar-mode
   (if (and keycast-tab-bar-mode
            tab-bar-mode)
       -1
     (or toggle :toggle))))

(defun aba/toggle-keycast--modeline (&optional toggle)
  "Toggles keycast on/off on the tab-bar."
  (when (eq toggle nil)
    (setq toggle -1))
  (let ((mode
         (pcase toggle
           ((pred (> 0)) +1)
           ((pred (<= 0)) -1)
           (t +1)
           ('toggle (not keycast-mode))
           (_ -1))))
    (if (and tab-bar-mode
             keycast-tab-bar-mode)
        (keycast-mode -1)
      (when (and (not (eq keycast-mode mode))
                 (not doom-modeline-mode))
        (keycast-mode mode)))))
  
(defun aba/toggle-keycast (&optional toggle)
  "Toggles keycast mode on/off on the modeline."
  (interactive)
  (aba/toggle-keycast--modeline
   (not (aba/toggle-keycast--tab-bar toggle))))

(advice-add 'tab-bar-mode :after 'aba/toggle-keycast)
;; (advice-remove 'tab-bar-mode 'aba/toggle-keycast)
(advice-add 'doom-modeline-mode :after 'aba/toggle-keycast)
;; (advice-remove 'doom-modeline-mode 'aba/toggle-keycast)
(global-set-key (kbd "C-<f12>") #'aba/toggle-keycast)

(provide 'config-screencast)
;;; config-screencast.el ends here
