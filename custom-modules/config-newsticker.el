;;; config-newsticker.el --- Configuración para newsticker  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Alejandro Barocio A.

;; Author: Alejandro Barocio A. <abarocio80@gmail.com>
;; Keywords: news

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(setq newsticker-url-list
      `(("PlanetGNU" "https://planet.gnu.org/atom.xml" nil 3600)
        ("GTY: Drawing near" "http://feeds.gty.org/GTYDrawingNear" nil ,(* 60 60 12))
        ("GTY: Blog" "http://feeds.gty.org/GTYBlog" nil ,(* 60 60 12))
        ("GTY: Strength For Today" "http://feeds.gty.org/GTYStrengthForToday" nil ,(* 60 60 12))
        ("GTY: The MacArthur Dily Bible" "http://feeds.gty.org/GTYDailyBible" nil ,(* 60 60 12))
        ("GTY: Daily Readings from the Life of Christ, Vol. 1" "http://feeds.gty.org/GTYDailyReadingsOne" nil ,(* 60 60 12))
        ))

(provide 'config-newsticker)
;;; config-newsticker.el ends here
