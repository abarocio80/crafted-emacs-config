;;; config-project.el --- Configuración para mis proyectos.  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Alejandro Barocio A.

;; Author: Alejandro Barocio A. <abarocio80@gmail.com>
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Mobulo para configuración para los directorios de proyectos.

;;; Code:

(require 'aba-project)

(project--read-project-list)

(project-remember-projects-under "~/Projects/code/")
(project-remember-projects-under "~/Projects/videos/")
(project-remember-projects-under "~/Projects/writing/")

(unless (member (aba/path-comptact-home user-emacs-directory)
                (project-known-project-roots))
  (project-remember-project (aba/path-comptact-home user-emacs-directory)))
(unless (member (aba/path-comptact-home crafted-config-path)
                (project-known-project-roots))
  (project-remember-project (aba/path-comptact-home crafted-config-path)))

(project--write-project-list)

(with-eval-after-load 'config-tab-bar
  (require 'config-project-tab))

(provide 'config-project)
;;; config-project.el ends here
