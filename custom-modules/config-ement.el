;;; config-ement.el --- Configuración de 'ement.el'  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Alejandro Barocio A.

;; Author: Alejandro Barocio A. <abarocio80@gmail.com>
;; Keywords: comm

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:


;;; Dependencias
(crafted-package-install-package
 '(ement :type git :host github :repo "alphapapa/ement.el"))

(setq plz-connect-timeout 120
      aba/ement-tab-name "Matrix:ement")

;; (require 'plz)
;; (require 'ts)
(require 'ement)
;; (require 'ement-api)
;; (require 'ement-macros)
;; (require 'ement-notify)
;; (require 'ement-room-list)
;; (require 'ement-room)
;; (require 'ement-structs)
;; (require 'ement-taxy)


;;; Variables
(custom-set-variables
 '(ement-save-sessions t))

(defvar aba/ement-sessions nil
  "Wether we have an active matrix session.")

(setq ement-after-initial-sync-hook
      '(;; ement-list-rooms
        (lambda (&rest _ignore)
          (ement-taxy-room-list))
        ement-view-initial-rooms
        ement--link-children)

      ement-notify-sound (string-trim (shell-command-to-string "find /usr/share -name \"message-new-instant.*\" 2>/dev/null")))

;; Open Ement Rooms at the right
(add-to-list 'display-buffer-alist
             '("\\*Ement Room"
               (display-buffer-in-side-window)
               (side . right)
               (window-width . 0.5)
               (window-height . fit-window-to-buffer)))

(defvar aba/exwm-ement-workspace 4
  "Workspace to display Matrix on EXWM.")

(require 'config-tab-bar)
(defun aba/ement-tab ()
  (when (and (fboundp 'aba/exwm-monitor-get)
             (string-equal "exwm" (getenv "WM")))
    (exwm-workspace-switch-create 4))
  (aba/tab-bar-switch-to-tab-ensure-unique aba/ement-tab-name)
  (tab-bar-change-tab-group "Comm"))

(defun aba/ement ()
  "Connects to my matrix session."
  (interactive)
  (aba/ement-tab)
  (if ement-sessions
      (ement-taxy-room-list)
    (progn
      (ement-connect :user-id  (password-store-get-field "matrix/default" "user")
                     :password (password-store-get       "matrix/default")))))

(let ((ement-id-list nil))
  (dolist (file (directory-files (expand-file-name "matrix" (getenv "PASSWORD_STORE_DIR")) nil ".gpg$"))
    (when (string-prefix-p "@" file)
      (add-to-list 'ement-id-list (file-name-base file) t)))
  ement-id-list)

(defun aba/ement-session-list ()
  "Builds a list for session names out from `ement-sessions'."
  (let ((sessions-list nil))
    (dolist (session ement-sessions)
      (let ((session-name (car session)))
        (when session-name
          (add-to-list 'sessions-list session-name t))))
    sessions-list))

(defun aba/ement-session-select (&optional sessions)
  "Interactively selects a session from `aba/ement-session-list'."
  (completing-read "Session: "
                   (or sessions
                       (aba/ement-session-list))))

(defun aba/ement-disconnect ()
  (interactive)
  (when (and ement-sessions
             (nth 0 ement-sessions))
    (let* ((sessions-list (aba/ement-session-list))
           (session-name
            (if (nth 1 sessions-list)
                (aba/ement-session-select sessions-list)
             sessions-list))
           (session (assoc session-name ement-sessions)))
      (when session
        (ement-disconnect (car session))))))

(defun aba/ement-quit ()
  (interactive)
  (aba/ement-disconnect)
  (aba/tab-bar-close-tab-by-name aba/ement-tab-name))


;;; Key-bindings
(defcustom crafted-config-ement-key "M"
  "Configure the leader-key key for `config-ement-*' modules."
  :group 'crafted-config
  :type 'key)

(global-unset-key (kbd (format "%s %s" crafted-config-leader-key crafted-config-ement-key)))

(defalias 'aba/ement-keymap (make-sparse-keymap))

(defvar aba/ement-map (symbol-function 'aba/ement-keymap)
  "Global keymap for characters following .")
(define-key leader-map (kbd crafted-config-ement-key) 'aba/ement-keymap)

(define-key aba/ement-map (kbd "c") #'aba/ement)
(define-key aba/ement-map (kbd "d") #'aba/ement-disconnect)
(define-key aba/ement-map (kbd "q") #'aba/ement-quit)

(define-key ement-room-mode-map (kbd "i") #'ement-room-send-image)

(provide 'config-ement)
;;; config-ement.el ends here
