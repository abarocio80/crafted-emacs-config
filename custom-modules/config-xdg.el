;;; config-xdg.el --- XDG Configuration            -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Alex Barocio

;; Author: Alejandro Barocio A. <abarocio80@gmail.com>
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; (expand-file-name "~/.config/")

;;; Code:

(unless (getenv "XDG_CONFIG_HOME")
  (setenv "XDG_CONFIG_HOME" (expand-file-name "~/.config/")))

(unless (getenv "XDG_CACHE_HOME")
  (setenv "XDG_CACHE_HOME" (expand-file-name "~/.cache/")))

(unless (getenv "XDG_DATA_HOME")
  (setenv "XDG_DATA_HOME" (expand-file-name "~/.local/share/")))

(defvar config-xdg-config-home (getenv "XDG_CONFIG_HOME")
  "")

(defvar config-xdg-cache-home (getenv "XDG_CACHE_HOME")
  "")

(defvar config-xdg-data-home (getenv "XDG_DATA_HOME")
  "")

(provide 'config-xdg)
;;; config-xdg.el ends here
