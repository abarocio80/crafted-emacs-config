;;; config-mu4e.el --- Configuración para cliente de correo mu4e  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Alex Barocio

;; Author: Alejandro Barocio A. <abarocio80@gmail.com>
;; Keywords: mail

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; `mu4e' es el cliente para emacs de `mu', un programa para indexar
;; correos en `maildir's.

;;; Code:


;;; Generalidades

;; Necesario para integrar con las contraseñas de mi depósito de
;; contraseñas.
(require 'config-password-store)

;; Preferir desplegar el mensaje en texto plano; es decir,
;; desalentamos a emacs a mostrarnos el mensaje en HTML o en RTF.
(with-eval-after-load "mm-decode"
  (add-to-list 'mm-discouraged-alternatives "text/html")
  (add-to-list 'mm-discouraged-alternatives "text/richtext"))

;; Configuramos algunas variables de utilería.
(setq aba/mu4e-tab-name "Mail:mu4e"
      user-mail-address "alex@localhost"
      aba/gmail-smtp-server  "smtp.gmail.com"
      aba/gmail-smtp-service 587 ; 587 465
      aba/gmail-smtp-stream  'starttls ; 'starttls 'ssl
      mml-secure-openpgp-sign-with-sender t
      
      ;; Personal
      aba/personal-user-full-name "Alejandro Barocio A."
      aba/mail-personal-address (password-store-get-field "mail/personal" "account")
      aba/mail-personal-signature
      (concat (format "%s\n" aba/personal-user-full-name)
              "Ex nihilo nihil fit.")
      aba/personal-sign-key "AA326E1B3DBAABC583D612ED82F322C099629533"

      ;; Work
      aba/mail-work-address (password-store-get-field "mail/work" "account")
      aba/work-user-full-name "Prof. Alejandro Barocio Alvarez"
      aba/mail-work-signature
      (concat (format "%s\n" aba/work-user-full-name)
              "Docente de Lengua y Literatura\n"
              "Escuela Secundaria Técnica No. 80")
      aba/work-sign-key "7B9F0706A045FD17E7592A2350D644E2EAB638BE")

;; Agregamos firma digital a todos los correos salientes
(add-hook 'message-send-hook 'mml-secure-message-sign-pgpmime)

;; Este paquete se instala (en Arch Linux y otras distros) al instalar
;; el paquete `mu'.
(require 'mu4e) ; La biblioteca principal.
(require 'mu4e-org) ; Para utilizar `org-links' con `org-mode'.


;;; Contextos

;; Contexto personal:
(setq aba/mu4e-context-personal
      (make-mu4e-context
       :name "Personal"
       :match-func (lambda (msg)
                     (when msg
                       (or
                        (mu4e-message-contact-field-matches msg
                                                            :to aba/mail-personal-address)
                        (string-prefix-p "/personal"
                                         (mu4e-message-field msg :maildir)))))
       :vars `((user-mail-address            . ,aba/mail-personal-address)
               (user-full-name               . ,aba/personal-user-full-name)
               (mu4e-compose-signature       . ,aba/mail-personal-signature)
               (mu4e-drafts-folder           . "/personal/[Gmail]/Drafts")
               (mu4e-sent-folder             . "/personal/[Gmail]/Sent Mail")
               (mu4e-refile-folder           . "/personal/[Gmail]/All Mail")
               (mu4e-trash-folder            . "/personal/[Gmail]/Trash")
               (smtpmail-smtp-server         . ,aba/gmail-smtp-server)
               (smtpmail-smtp-service        . ,aba/gmail-smtp-service)
               (smtpmail-stream-type         . ,aba/gmail-smtp-stream)
               (mml-secure-openpgp-signers   . (,aba/personal-sign-key)))))

;; Contexto de trabajo:
(setq aba/mu4e-context-work
      (make-mu4e-context
       :name "Work"
       :match-func (lambda (msg)
                     (when msg
                       (or
                        (mu4e-message-contact-field-matches msg
                                                            :to aba/mail-work-address)
                        (string-prefix-p "/work"
                                         (mu4e-message-field msg :maildir)))))
       :vars `((user-mail-address            . ,aba/mail-work-address)
               (user-full-name               . ,aba/work-user-full-name)
               (mu4e-compose-signature       . ,aba/mail-work-signature)
               (mu4e-drafts-folder           . "/work/[Gmail]/Drafts")
               (mu4e-sent-folder             . "/work/[Gmail]/Sent Mail")
               (mu4e-refile-folder           . "/work/[Gmail]/All Mail")
               (mu4e-trash-folder            . "/work/[Gmail]/Trash")
               (smtpmail-smtp-server         . ,aba/gmail-smtp-server)
               (smtpmail-smtp-service        . ,aba/gmail-smtp-service)
               (smtpmail-stream-type         . ,aba/gmail-smtp-stream)
               (mml-secure-openpgp-signers   . (,aba/work-sign-key)))))

;; Configuramos distintos contextos para distintas cuentas.
(setq mu4e-contexts `(,aba/mu4e-context-personal
                      ,aba/mu4e-context-work))


;;; Cambiamos algo de el comportamiento por defecto

;; Cambiamos el comportamiento por default para el contexto.
(setq mu4e-context-policy 'pick-first)

;; Cambiamos el comportamiento por defecto para el contexto cuando
;; redactamos un mensaje.
(setq mu4e-compose-context-policy 'ask)

;; Configuramons la función a utilizar para enviar correo.
(setq message-send-mail-function 'smtpmail-send-it)

;; Cambiamos esta variable al valor `t' para evitar problemas de
;; syncronización cuando usamos mbsync.
(setq mu4e-change-filenames-when-moving t)

;; Verificamos si hay correo nuevo cada 10 minutos con mbsync.
(setq mu4e-update-interval  (* 10 60)
      mu4e-get-mail-command "mbsync --config ~/.config/isync/mbsyncrc -a")


;;; Cambiamos elementos básicos de la interfaz
;; Creamos algunos atajos.
(setq mu4e-maildir-shortcuts
      '(("/personal/Inbox"             . ?i)
        ("/personal/Banorte"           . ?b)
        ("/personal/[Gmail]/Sent Mail" . ?s)
        ("/personal/[Gmail]/Trash"     . ?t)
        ("/personal/[Gmail]/Drafts"    . ?d)
        ("/personal/[Gmail]/All Mail"  . ?a)
        ("/work/Inbox"                 . ?I)
        ("/work/[Gmail]/Sent Mail"     . ?S)
        ("/work/[Gmail]/Trash"         . ?T)
        ("/work/[Gmail]/Drafts"        . ?D)
        ("/work/[Gmail]/All Mail"      . ?A)))

;; Establecemos algunos marcadores.
(setq mu4e-bookmarks
      '((:name "Unread messages" :query "flag:unread AND NOT flag:trashed" :key ?i)
        (:name "Today's messages" :query "date:today..now" :key ?t)
        (:name "Subdirectora" :query "from:aleyda OR to:aleyda" :key ?b)
        (:name "Coordinador" :query "from:gregorio OR to:gregorio" :key ?s)
        (:name "Last 7 days" :query "date:7d..now" :hide-unread t :key ?w)
        (:name "Messages with images" :query "mime:image/*" :key ?p)))



;;; Enviar correo con múltiples cuentas del mismo servidor
;; Guardamos las cuentas en alguna estructura.
(setq smtp-accounts nil)
(push `(,aba/mail-personal-address ,aba/gmail-smtp-server ,aba/gmail-smtp-service ,aba/mail-personal-address) smtp-accounts)
(push `(,aba/mail-work-address ,aba/gmail-smtp-server ,aba/gmail-smtp-service ,aba/mail-work-address) smtp-accounts)

;; Definimos una función que sobreescriba los encabezados (del correo)
;; de acuerdo al encabezado "From".
(defun set-smtp-server-message-send-and-exit ()
  "Set SMTP server from list of multiple ones and send mail."
  (interactive)
  (message-remove-header "X-Message-SMTP-Method")
  (let ((sender
         (nth 1 (mail-extract-address-components (message-fetch-field "From")))))
    (dolist (account smtp-accounts)
      (let ((addr (nth 0 account))
            (server (nth 1 account))
            (port (nth 2 account))
            (user (nth 3 account)))
        (when (string-match addr sender)
          (message-add-header (format "X-Message-SMTP-Method: smtp %s %d %s" server port user))
          )))
    (let ((xmess
           (message-fetch-field "X-Message-SMTP-Method")))
      (if xmess
          (progn
            (message (format "Sending message using '%s' with config '%s'" sender xmess))
            (message-send-and-exit))
        (error "Could not find SMTP Server for this Sender address: %s. You might want to correct it or add it to the SMTP Server list 'smtp-accounts'" sender)))))

;; Definimos una función que redefina los atajos de teclado locales
;; (en modo de composición).
(defun local-mu4e-compose-mode-setup ()
  "Keys."
  (local-set-key (kbd "C-c C-c") 'set-smtp-server-message-send-and-exit))

;; Enganchamos la función anterior en el gancho de composición.
(add-hook 'mu4e-compose-mode-hook 'local-mu4e-compose-mode-setup)

(require 'config-tab-bar)
(defun aba/mu4e-tab ()
  (when (and (fboundp 'aba/exwm-monitor-get)
             (string-equal "exwm" (getenv "WM")))
    (exwm-workspace-switch-create 4))
  (aba/tab-bar-switch-to-tab-ensure-unique aba/mu4e-tab-name)
  (tab-bar-change-tab-group "Comm"))

(defun aba/mu4e (&optional o)
  "Inicia mu4e en su correspondiente pestaña (en `tab-bar')."
  (interactive)
  (aba/mu4e-tab)
  (mu4e o))

(defun aba/mu4e-compose-new ()
  "Crear un correo nuevo desde la pestaña correspondiente."
  (interactive)
  (aba/mu4e-tab)
  (mu4e-compose-new))

(defun aba/mu4e-context-switch ()
  "Cambiamos de contexto en la pestaña correspondiente."
  (interactive)
  (aba/mu4e-tab)
  (mu4e-context-switch))

(defun aba/mu4e-update-mail-and-index ()
  "Sincronizamos el correo en la pestaña correspondiente."
  (interactive)
  (aba/mu4e-tab)
  (mu4e-update-mail-and-index))

(defun aba/mu4e-quit ()
  "Asks to quit mu4e and closes the `aba/mu4e-tab-name' tab."
  (interactive)
  (message "aba/mu4e-quit")
  (mu4e-quit)
  (when (and (aba/kill-buffer mu4e--log-buffer-name)
               (aba/kill-buffer mu4e-main-buffer-name)
               (aba/kill-buffer mu4e-view-buffer-name)
               (aba/kill-buffer mu4e--sexp-buffer-name)
               (aba/kill-buffer mu4e-headers-buffer-name)
               (aba/kill-buffer mu4e-embedded-buffer-name)
               (aba/kill-buffer mu4e~view-raw-buffer-name))
    (aba/tab-bar-close-tab-by-name aba/mu4e-tab-name)))


;;; Key-bindings
(define-key mu4e-main-mode-map (kbd "q") #'aba/mu4e-quit)
(with-eval-after-load 'evil
  (evil-define-key 'normal mu4e-main-mode-map (kbd "q") #'aba/mu4e-quit))

(defcustom crafted-config-mu4e-key "m"
  "Configure the leader-key key for `config-mu4e-*' modules."
  :group 'crafted-config
  :type 'key)

(global-unset-key (kbd (format "%s %s" crafted-config-leader-key crafted-config-mu4e-key)))

(defalias 'aba/mu4e-keymap (make-sparse-keymap))

(defvar aba/mu4e-map (symbol-function 'aba/mu4e-keymap)
  "Global keymap for characters following .")
(define-key leader-map (kbd crafted-config-mu4e-key) 'aba/mu4e-keymap)

(define-key aba/mu4e-map (kbd "m") #'aba/mu4e) ;; Opens mu4e.
(define-key aba/mu4e-map (kbd "c") #'aba/mu4e-compose-new) ;; Composes new mail with mu4e.
(define-key aba/mu4e-map (kbd "s") #'aba/mu4e-context-switch) ;; Switches mu4e context.
(define-key aba/mu4e-map (kbd "u") #'aba/mu4e-update-mail-and-index) ;; Downloads mail and indexes it with mu.
(define-key aba/mu4e-map (kbd "q") #'aba/mu4e-quit) ;; Quits mu4e and closes its tab.

(provide 'config-mu4e)
;;; config-mu4e.el ends here
