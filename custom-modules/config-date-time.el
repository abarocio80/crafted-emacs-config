;;; config-date-time.el --- Configuración de fecha y hora.  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Alex Barocio

;; Author: Alejandro Barocio A. <abarocio80@gmail.com>
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(custom-set-variables
 '(display-time-interval 1)
 '(display-time-day-and-date t)
 '(display-time-format "%a %d %b, %Y, %T"))

(display-time-mode +1)

(provide 'config-date-time)
;;; config-date-time.el ends here
