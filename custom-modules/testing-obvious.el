;;; testing-obvious.el -- Testing alphapapa's obvious.el -*- lexical-binding: t -*-
;; https://github.com/alphapapa/obvious

(when (eq crafted-package-system 'straight)
  (crafted-package-install-package
   '(obvious :type git
             :host github
             :repo "alphapapa/obvious.el"))

  ;; Maybe this isn't needed, but I rather be cautious.
  (require 'obvious)

  ;; Lets hide the heavily
  (setq obvious-headers nil)
  
  ;; We set a global binding
  (global-set-key (kbd "C-c o") #'obvious-mode)
  
  )

(provide 'testing-obvious)
;;; testing-obvious.el ends here
