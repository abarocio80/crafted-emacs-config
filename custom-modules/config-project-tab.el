;;; config-project-tab.el --- Configuración para abrir proyectos en nuevos grupos de tabs.  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Alejandro Barocio A.

;; Author: Alejandro Barocio A. <abarocio80@gmail.com>
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Abrimos projectos nuevos (project.el) utilizando un nuevo tab-bar-group.

;;; Code:

;; (straight-use-package 'project-tab-groups)
(crafted-package-install-package 'project-tab-groups)
(project-tab-groups-mode +1)

(provide 'config-project-tab)
;;; config-project-tab.el ends here
