;;; config-env.el --- Configuración de variables de ambiente  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Alex Barocio

;; Author: Alejandro Barocio A. <abarocio80@gmail.com>
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Por alguna razón, emacs daemon (iniciado con `systemctl --user') no
;; toma correctamente las variables de entorno.

;; También este es el lugar en que se pueden definir variables de
;; entorno que necesiten customización en emacs.

;;; Code:

(defun defenv (variable value)
  "Sets the environment VARIABLE as VALUE, if not already set."
  (unless (getenv variable)
    (setenv variable value))
  (getenv variable))

(defenv "HOME" "/home/alex")

(load-file (expand-file-name "custom-modules/config-xdg.el" crafted-config-path))

(defenv "PASSWORD_STORE_DIR" (expand-file-name "pass" config-xdg-data-home))
(defenv "GNUPGHOME" (expand-file-name "gnupg" config-xdg-data-home))

(provide 'config-env)
;;; config-env.el ends here
