;;; config-password-store.el --- Configuración para utilizar `password-store'  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Alex Barocio

;; Author: Alejandro Barocio A. <abarocio80@gmail.com>
;; Keywords: convenience, tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(require 'config-leader-key)
(require 'config-xdg)

(crafted-package-install-package 'password-store)
(require 'password-store)

(defvar user-data-directory (expand-file-name "emacs" config-xdg-data-home))

(setq auth-sources
      `('password-store
        ,(expand-file-name "authinfo.gpg" user-data-directory)))
;; (add-to-list 'auth-sources (expand-file-name "authinfo.gpg" user-data-directory))
(setq auth-source-debug t)

;; Integramos pass con auth-sources
(setq auth-source-pass-filename (getenv "PASSWORD_STORE_DIR"))

(require 'epa-file)
;; (epa-file-enable)
(setq epg-pinentry-mode 'loopback)

;;; Funciones extra
(defun aba/password-store-insert-password ()
  "Inserts password ENTRY at point."
  (interactive)
  (insert (password-store-get (completing-read "Select entry: " (password-store-list)))))

(defun aba/password-store-insert-field (&optional field)
  "Inserts password ENTRY at point."
  (insert (password-store-get-field (completing-read "Select entry: " (password-store-list))
                                    (or field (read-string "Select field: " )))))

(defun aba/password-store-insert-url ()
  "Inserts password ENTRY at point."
  (insert (aba/password-store-get-field ("url"))))


;;; keymap
(defcustom crafted-config-password-key "p"
  "Configure the leader-key key for `config-*' modules."
  :group 'aba/config
  :type 'key)

(defalias 'aba/password-store-keymap (make-sparse-keymap))

(defvar aba/password-store-map (symbol-function 'aba/password-store-keymap)
  "Global keymap for characters following .")

(define-key leader-map (kbd crafted-config-password-key) 'aba/password-store-keymap)

(define-key aba/password-store-map (kbd "p") #'password-store-copy)
(define-key aba/password-store-map (kbd "u") #'password-store-url)
(define-key aba/password-store-map (kbd "f") #'password-store-copy-field)
(define-key aba/password-store-map (kbd "i") #'password-store-insert)
(define-key aba/password-store-map (kbd "I p") #'aba/password-store-insert-password)
(define-key aba/password-store-map (kbd "I u") #'aba/password-store-insert-url)
(define-key aba/password-store-map (kbd "I f") #'aba/password-store-insert-field)
(define-key aba/password-store-map (kbd "g") #'password-store-generate)
(define-key aba/password-store-map (kbd "e") #'password-store-edit)

(provide 'config-password-store)
;;; config-password-store.el ends here
