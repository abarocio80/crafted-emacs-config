;;; config-gnus.el --- Configuración para Gnus       -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Alex Barocio

;; Author: Alejandro Barocio A. <abarocio80@gmail.com>
;; Keywords: mail, news

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

;; Cambiamos la ubicación del archivo de Gnus.
(setq gnus-init-file (expand-file-name "gnus-config.el" crafted-config-path))

;; (add-to-list 'auth-sources (expand-file-name "emacs/authinfo.gpg" config-xdg-data-home))
(add-to-list 'auth-sources (expand-file-name "emacs/authinfo" config-xdg-data-home))

(define-key leader-map (kbd "g") #'gnus)
;; (define-key leader-map (kbd "S-g") #'gnus-child)
;; (define-key leader-map (kbd "M-g") #'gnus-other-frame)

(provide 'config-gnus)
;;; config-gnus.el ends here
