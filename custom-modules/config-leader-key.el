;;; config-leader-key.el --- Configuración de mi leader-key  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Alex Barocio

;; Author: Alejandro Barocio A. <abarocio80@gmail.com>
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(defcustom crafted-config-leader-key "C-\\"
  "Configure the leader-key key for `config-*' modules."
  :group 'aba/config
  :type 'key)

(defalias 'leader-keymap (make-sparse-keymap))

(defvar leader-map (symbol-function 'leader-keymap)
  "Global keymap for characters following .")

;; Desactivamos el prefijo si está activado.
(global-unset-key (kbd crafted-config-leader-key))

(define-key global-map (kbd crafted-config-leader-key) 'leader-keymap)

(provide 'config-leader-key)
;;; config-leader-key.el ends here
