;;; config-vterm.el --- Configuración para vterm     -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Alex Barocio

;; Author: Alejandro Barocio A. <abarocio80@gmail.com>
;; Keywords: terminals

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(crafted-package-install-package 'vterm)
(crafted-package-install-package 'xterm-color)

(setq vterm-max-scrollback 10000)

;; Configure vterm for evil-mode
(with-eval-after-load 'evil
  ;; Make sure that entering insert mode positions the cursor correctly
  (advice-add 'evil-collection-vterm-insert :before #'vterm-reset-cursor-point))

(global-set-key (kbd "S-C-M-<return>") 'vterm)
(define-key leader-map (kbd "S-<return>") 'vterm)

(provide 'config-vterm)
;;; config-vterm.el ends here
