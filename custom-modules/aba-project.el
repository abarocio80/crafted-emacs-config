;;; aba-project.el --- Manage creation of projects within emacs.  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Alejandro Barocio A.

;; Author: Alejandro Barocio A. <abarocio80@gmail.com>
;; Keywords: vc, files, tools, convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

;;; Dependencies
(require 'vc)
(require 'project)

;;; Variables

(defvar aba/project--backend-history '()
  "History of backend selection.")
(defvar aba/project-backend nil
  "Default backend to use.")
(defvar aba/project--history '()
  "History of projects.")
(defvar aba/project-text-flavor 'org
  "Prefered flavour of text files.")
(defvar aba/project--licenses-common-directory "/usr/share/licenses/common"
  "System directory where licenses could be found.")
(defvar aba/project-text-flavor nil
  "Flavor for the README file.  Options are `md', `org', `plain' or nil.")
(defvar aba/project-src-directory "src"
  "Name of the source directory.")
(defvar aba/project-lib-directory "lib"
  "Name of the library directory.")
(defvar aba/project-man-directory "man"
  "Name of the man directory.")
(defvar aba/project-test-directory "test"
  "Name of the test directory.")
(defvar aba/project-bin-directory "bin"
  "Name of the binary directory.")
(defvar aba/project-build-directory "build"
  "Name of the distribution directory.")

;; Initializing variables
(project--read-project-list)

;;; Functions

;; Manage project list

;; Replace $HOME value with '~' in the given path.
(defun aba/path-comptact-home (path)
  "Compacts the value of $HOME in PATH to '~'."
  (replace-regexp-in-string (format "^%s" (getenv "HOME")) "~" path))

;; Manage project creation

(defun aba/project-text-file-name (file-base &optional flavor)
  ""
  (format "%s%s" file-base
          (if (stringp flavor)
              flavor
            (pcase (or flavor
                       aba/project-text-flavor)
              ('org   ".org")
              ('md    ".md")
              ('plain ".txt")
              ('tex   ".tex")
              (_      "")))))

(defun aba/project-src-file-name (file)
  (let ((dir aba/project-src-directory))
    (unless (file-exists-p dir)
      (make-directory dir t))
    (f-touch (expand-file-name file dir))))

(defun aba/project-lib-file-name (file)
  (let ((dir aba/project-lib-directory))
    (unless (file-exists-p dir)
      (make-directory dir t))
    (f-touch (expand-file-name file dir))))

(defun aba/project-man-file-name (file)
  (let ((dir aba/project-man-directory))
    (unless (file-exists-p dir)
      (make-directory dir t))
    (f-touch (expand-file-name file dir))))

(defun aba/project-test-file-name (file)
  (let ((dir aba/project-test-directory))
    (unless (file-exists-p dir)
      (make-directory dir t))
    (f-touch (expand-file-name (format "test-%s" file) dir))))

(defun aba/project--licenses ()
  "Create a list of licenses available in the system."
  (let ((licenses '()))
    (dolist (license
             (s-split
              "\n"
              (s-trim
               (shell-command-to-string
                (format "find %s -name \"*.txt\"" aba/project--licenses-common-directory)))))
      (let* ((file (file-name-nondirectory license))
             (parent (car (reverse (f-split (file-name-directory license)))))
             (path (list parent)))
        (when (not (or (string-equal "license.txt" file)
                       (string-match-p "readme" file)))
          (add-to-list 'path (file-name-base file) t))
        (add-to-list 'licenses (s-join "/" path))))
    licenses))

(defun aba/project--license-select ()
  (let* ((licenseX
          (completing-read "Select a license: " (aba/project--licenses)))
         (license
          (expand-file-name
           (if (string-match-p "\\/" licenseX)
               (concat licenseX ".txt" )
             (concat licenseX "/license.txt"))
           aba/project--licenses-common-directory)))
    (when (not (file-exists-p license))
      (error "The license file %s doesn't exist!" license))
    license))

;;;###autoload
(defun aba/project-create (directory &optional backend)
  "Creates a new project in DIRECTORY."
  (interactive "GDirectory in which create the new project: ")
  (unless (file-exists-p directory)
    (make-directory directory t))
  (unless (file-directory-p directory)
    (error "%s is not a directory!"
           directory))
  (dired directory)
  (when (string-prefix-p (or (vc-root-dir)
                             "/nonexistant")
                         directory)
    (error "Directory %s is inside a directory under version control (%s)"
           directory
           (vc-root-dir)))
  (vc-create-repo (or backend
                      aba/project-backend
                      'git))
  (unless (file-exists-p (expand-file-name (aba/project-text-file "README") directory))
    (f-touch (expand-file-name (aba/project-text-file "README") directory)))
  (unless (file-exists-p (expand-file-name "LICENSE" directory))
    (when (y-or-n-p "Do you want to add a License to the project?")
      (f-copy (aba/project--license-select) (expand-file-name "LICENSE" directory))))
  (aba/project-add directory)
  (kill-this-buffer)
  (project-switch-project directory))



(provide 'aba-project)
;;; aba-project.el ends here
