;;; config-tab-bar.el --- Configuración para `tab-bar'.  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Alejandro Barocio A.

;; Author: Alejandro Barocio A. <abarocio80@gmail.com>
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(setq tab-bar-format '(tab-bar-format-history
                       tab-bar-format-tabs-groups
                       tab-bar-separator
                       tab-bar-add-tab
                       tab-bar-format-align-right
                       tab-bar-format-global)

      tab-bar-show t
      tab-bar-close-button-show nil)

(tab-bar-mode +1)
(tab-line-mode +1)

(defun aba/tab-bar-switch-to-tab-ensure-unique (name)
  "Switches to tab-bar named NAME, while ensuring that it exists.

If no tab exists with such name, it creates it, ensuring that
only one tab is created on that frame with this NAME."
  (unless (tab-bar--tab-index-by-name name)
    (tab-bar-switch-to-tab "Main")
    (tab-bar-new-tab 1000)
    (tab-bar-rename-tab name))
  (tab-bar-switch-to-tab name))

(defun list-index (element list &optional index)
  "Returns the index of ELEMENT in LIST."
  (if list
      (let ((i (or index 0)))
        (if (eq element (car list))
            i
          (list-index element (cdr list) (1+ i))))
    nil))

(defun aba/tab-bar-close-tab-by-name (name)
  (when (tab-bar--tab-index-by-name name)
    (tab-bar-close-tab-by-name name)))

(defun aba/tab-line--shift-tab (n)
  "Selects the tab N positions away in the window's tab-line.

If it shifts beyond the list limits, it wraps."
  (global-tab-line-mode +1)
  (let* ((tabs-list (tab-line-tabs-window-buffers))
         (tab (current-buffer))
         (count (length tabs-list))
         (pivot (list-index tab tabs-list))
         (to (+ pivot n)))
    (when (and tabs-list tab count pivot to)
      (if (< to 0)
          (progn
          (setq to (1- count)))
        (when (>= to count)
            (setq to 0)))
      (tab-line-select-tab-buffer (nth to tabs-list)))))

(defun aba/tab-line-switch-to-next-tab ()
  "Selects the previous tab in the window's tab-line.

If it is already in the first one, cicles over to the last one."
  (interactive)
  (aba/tab-line--shift-tab 1))

(defun aba/tab-line-switch-to-prev-tab ()
  "Selects the previous tab in the window's tab-line.

If it is already in the first one, cicles over to the last one."
  (interactive)
  (aba/tab-line--shift-tab -1))
    
(global-set-key (kbd "<f12>") #'tab-bar-mode)
(global-set-key (kbd "S-<f12>") #'global-tab-line-mode)

(global-set-key (kbd "C-<tab>") #'tab-bar-switch-to-next-tab)
(global-set-key (kbd "C-<iso-lefttab>") #'tab-bar-switch-to-prev-tab)

(global-set-key (kbd "C-`") #'aba/tab-line-switch-to-next-tab)
(global-set-key (kbd "C-~") #'aba/tab-line-switch-to-prev-tab)

(with-eval-after-load 'config-ement
  (define-key ement-taxy-mode-map (kbd "C-<tab>") #'tab-bar-switch-to-next-tab)
  (define-key ement-taxy-mode-map (kbd "C-S-<tab>") #'tab-bar-switch-to-prev-tab))

(setq aba/tab-bar--main-name "Main")

(when (selected-frame)
  (tab-bar-change-tab-group aba/tab-bar--main-name))
  
(add-hook 'server-after-make-frame-hook
          (lambda () (interactive)
            (tab-bar-change-tab-group (format "%s:%s" aba/tab-bar--main-name (string-replace "server " "" (format "%s" (car server-clients)))))))

(provide 'config-tab-bar)
;;; config-tab-bar.el ends here
