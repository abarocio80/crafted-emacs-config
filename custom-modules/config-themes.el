;;; config-themes.el --- Configuración para mis temas visuales  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Alejandro Barocio A.

;; Author: Alejandro Barocio A. <abarocio80@gmail.com>
;; Keywords: faces, convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Una configuración sencilla para tener una lista de selección de
;; temas (y alternativas) sobre los que podemos iterar.

;; Cada tema puede establecer una alternativa (pensando en aquellos
;; que les gusta tener temas claros y oscuros), y para cada tema
;; podemos elegir también la opción principal o la alternativa.

;;; Code:

(add-to-list 'custom-theme-load-path
             (expand-file-name "themes" crafted-config-path))
(crafted-package-install-package 'doom-themes)

;; Variables
(defvar aba/theme-selection-list '()
  "A list containing a selection of themes.

Each theme is a list in it self. Each theme-list contains two
themes, the first being the main one, and the second the
alternative (dark/light or light/dark)")

(defvar aba/theme-selection-index 0
  "A list containing a selection of themes.")

(defvar aba/theme-selection-alternative nil
  "Wether use the alternative theme variant.")


;;; Functions

(defun aba/theme-selection--current ()
  "Return the current selected theme/variant."
  (when (>= aba/theme-selection-index (length aba/theme-selection-list))
    (setq aba/theme-selection-index 0))
  (when (< aba/theme-selection-index 0)
    (setq aba/theme-selection-index (1- (length aba/theme-selection-list))))
  (let* ((theme (nth aba/theme-selection-index
                     aba/theme-selection-list))
         (main (car theme))
         (alt (cadr theme)))
    (or (and aba/theme-selection-alternative
             alt)
        main)))
;; (aba/theme-selection--current)

(defun aba/tab-bar-face (&rest _ignore)
  (interactive)
  ;; tab-bar
    (when (featurep 'tab-bar)
      ;; Tabs
      (set-face-attribute 'tab-bar nil
                          :background (plist-get (custom-face-attributes-get
                                                  'mode-line nil)
                                                 :background)
                          :foreground (plist-get (custom-face-attributes-get
                                                  'mode-line nil)
                                                 :foreground))
      ;; tab-bar-tab (current)
      (set-face-attribute 'tab-bar-tab nil
                          :background (plist-get (custom-face-attributes-get
                                                  'mode-line-inactive nil)
                                                 :background)
                          :foreground (plist-get (custom-face-attributes-get
                                                  'mode-line-inactive nil)
                                                 :foreground)
                          :slant 'normal
                          :weight 'bold)
      ;; tab-bar-tab-inactive
      (set-face-attribute 'tab-bar-tab-inactive nil
                          :background (plist-get (custom-face-attributes-get
                                                  'mode-line nil)
                                                 :background)
                          :foreground (plist-get (custom-face-attributes-get
                                                  'mode-line nil)
                                                 :foreground)
                          :slant 'italic
                          :weight 'light)
      ;; Groups
      ;; tab-bar-tab-group-current
      (set-face-attribute 'tab-bar-tab-group-current nil
                          :background (plist-get (custom-face-attributes-get
                                                  'mode-line-inactive nil)
                                                 :background)
                          :foreground (plist-get (custom-face-attributes-get
                                                  'mode-line-inactive nil)
                                                 :foreground)
                          :weight 'ultra-bold)
      ;; tab-bar-tab-group-inactive
      (set-face-attribute 'tab-bar-tab-group-inactive nil
                          :background (plist-get (custom-face-attributes-get
                                                  'mode-line nil)
                                                 :background)
                          :foreground (plist-get (custom-face-attributes-get
                                                  'mode-line nil)
                                                 :foreground)
                          :slant 'italic
                          :weight 'normal))
    ;; tab-line
    (when (featurep 'tab-line)
      ;; Tabs
      (set-face-attribute 'tab-line nil
                          :background (plist-get (custom-face-attributes-get
                                                  'mode-line-inactive nil)
                                                 :background)
                          :foreground (plist-get (custom-face-attributes-get
                                                  'mode-line-inactive nil)
                                                 :foreground))
      ;; tab-line-tab-current
      (set-face-attribute 'tab-line-tab-current nil
                          :background (plist-get (custom-face-attributes-get
                                                  'mode-line nil)
                                                 :background)
                          :foreground (plist-get (custom-face-attributes-get
                                                  'mode-line nil)
                                                 :foreground)
                          :weight 'bold)
      ;; tab-line-tab-inactive
      (set-face-attribute 'tab-line-tab-inactive nil
                          :background (plist-get (custom-face-attributes-get
                                                  'mode-line-inactive nil)
                                                 :background)
                          :foreground (plist-get (custom-face-attributes-get
                                                  'mode-line-inactive nil)
                                                 :foreground)
                          :weight 'light)))

(with-eval-after-load 'tab-bar
  (aba/tab-bar-face))

(with-eval-after-load 'tab-line
  (aba/tab-bar-face))

(advice-add 'enable-theme :after 'aba/tab-bar-face)


;;; Commands

;;;###autoload
(defun aba/theme-selection-enable-current ()
  "Enables the current (variant) theme."
  (interactive)
  (dolist (old custom-enabled-themes)
    (disable-theme old))
  (let ((current (aba/theme-selection--current)))
    (message "Enabling theme '%s'." (symbol-name current))
    (pcase current
      ('modus-vivendi (modus-themes-load-vivendi))
      ('modus-operandi (modus-themes-load-operandi))
      (_ (when (custom-theme-p current )
           (enable-theme current))))))
;; (aba/theme-selection-enable-current)

;;;###autoload
(defun aba/theme-selection-next ()
  "Selects the next theme from `aba/theme-selection-list'."
  (interactive)
  (let ((count (length aba/theme-selection-list))
        (next (+ aba/theme-selection-index 1)))
    (setq aba/theme-selection-index
          (if (>= next count)
              0
            next)))
  (aba/theme-selection-enable-current))

;;;###autoload
(defun aba/theme-selection-prev ()
  "Selects the previous theme from `aba/theme-selection-list'."
  (interactive)
  (let ((count (length aba/theme-selection-list))
        (prev (- aba/theme-selection-index 1)))
    (setq aba/theme-selection-index
          (if (< prev 0)
              (- count 1)
            prev)))
  (aba/theme-selection-enable-current))

;;;###autoload
(defun aba/theme-selection-toggle-variant ()
  "Toggles the theme variant used."
  (interactive)
  (setq aba/theme-selection-alternative (not aba/theme-selection-alternative))
  (aba/theme-selection-enable-current)
  (message "Toggled theme alternative: %s" (aba/theme-selection--current)))

;;;###autoload
(defun aba/theme-selection (themes)
  "Adds themes in THEMES to `aba/theme-selection-list'.

THEMES is meant to be a list of themes."
  (setq aba/theme-selection-list themes
        aba/theme-selection-index 0)
  (dolist (theme themes)
    (dolist (th theme)
      (unless (custom-theme-p th)
        (load-theme th t t)))
    (add-to-list 'aba/theme-selection-list theme t))
  (aba/theme-selection-enable-current)
  aba/theme-selection-list)

(provide 'config-themes)
;;; config-themes.el ends here
