;;; config-modus.el --- Configuración de modus-themes   -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Alejandro Barocio A.

;; Author: Alejandro Barocio A. <abarocio80@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Aquí configuro algunos de los switches de `modus-themes', de
;; acuerdo a la documentación oficial del paquete (ver info::Modus
;; Themes).

;;; Code:

;; Verificamos la versión de emacs para saber si `modus-themes' ya
;; viene instalado o si lo tenemos que instalar.  Si la versión es
;; menor a 28, entonces necesitamos intalar el paquete.
;; --- (For some reason I need to install this even in emacs 28.1).
(when (version< emacs-version "28")
  (rational-package-install-package 'modus-themes))
;; (straight-use-package 'modus-themes)

;; (require 'modus-themes)

;; Aquí es donde modificamos el comportamiento de los temas de
;; `modus-themes'.
(setq modus-themes-italic-constructs t
      modus-themes-bold-constructs t
      modus-themes-no-mixed-fonts nil
      modus-themes-subtle-line-numbers t
      modus-themes-success-deuteranopia nil
      modus-themes-tabs-accented nil
      modus-themes-inhibit-reload t ; only applies to
                                    ; `customize-set-variable' and
                                    ; related

      modus-themes-fringes 'subtle ; {nil,'subtle,'intense}

      ;; Options for `modus-themes-lang-checkers' are either nil (the
      ;; default), or a list of properties that may include any of
      ;; those symbols: `straight-underline', `text-also',
      ;; `background', `intense'
      modus-themes-lang-checkers '(intense)

      ;; Options for `modus-themes-mode-line' are either nil, or a
      ;; list that can combine any of `3d' OR `moody', `borderless',
      ;; `accented', `padded'.
      modus-themes-mode-line '(borderless)

      ;; Options for `modus-themes-syntax' are either nil (the
      ;; default), or a list of properties that may include any of
      ;; those symbols: `faint', `yellow-comments', `green-strings',
      ;; `alt-syntax'
      modus-themes-syntax '(yellow-comments green-strings)

      ;; Options for `modus-themes-hl-line' are either nil (the
      ;; default), or a list of properties that may include any of
      ;; those symbols: `accented', `underline', `intense'
      modus-themes-hl-line '(accented intense)

      ;; Options for `modus-themes-paren-match' are either nil (the
      ;; default), or a list of properties that may include any of
      ;; those symbols: `bold', `intense', `underline'
      modus-themes-paren-match '(bold intense)

      ;; Options for `modus-themes-links' are either nil (the
      ;; default), or a list of properties that may include any of
      ;; those symbols: `neutral-underline' OR `no-underline', `faint'
      ;; OR `no-color', `bold', `italic', `background'
      modus-themes-links '(neutral-underline background)

      ;; Options for `modus-themes-prompts' are either nil (the
      ;; default), or a list of properties that may include any of
      ;; those symbols: `background', `bold', `gray', `intense',
      ;; `italic'
      modus-themes-prompts '(intense bold)

      modus-themes-completions
      '((matches . (italic background intense))
        (selection . (extrabold accented intense))
        (popup . (accented intense)))

      modus-themes-mail-citations 'faint ; {nil,'faint,'monochrome}

      ;; Options for `modus-themes-region' are either nil (the
      ;; default), or a list of properties that may include any of
      ;; those symbols: `no-extend', `bg-only', `accented'
      modus-themes-region '(bg-only no-extend)

      ;; Options for `modus-themes-diffs': nil, 'desaturated,
      ;; 'bg-only, 'deuteranopia, 'fg-only-deuteranopia
      modus-themes-diffs nil ; 'bg-only

      modus-themes-org-blocks 'gray-background ; {nil,'gray-background,'tinted-background}

      modus-themes-org-agenda ; this is an alist: read the manual or
                              ; its doc string
      '((header-block . (variable-pitch scale-title))
        (header-date . (grayscale workaholic bold-today))
        (event . (accented scale-small))
        (scheduled . uniform)
        (habit . traffic-light-deuteranopia))

      modus-themes-headings ; this is an alist: read the manual
      '((1 . (overline))    ; or its doc string
        (2 . (rainbow overline))
        (t . (no-boldx)))

      modus-themes-variable-pitch-ui nil
      modus-themes-variable-pitch-headings t
      modus-themes-scale-headings t
      modus-themes-scale-1 1.1
      modus-themes-scale-2 1.15
      modus-themes-scale-3 1.21
      modus-themes-scale-4 1.27
      modus-themes-scale-title 1.33)

(load-theme 'modus-vivendi t)

;; Cargamos los temas.
(modus-themes-load-themes)

(provide 'config-modus)
;;; config-modus.el ends here
