;;; Personal Crafted Emacs Config
;;; Author: Alejandro Barocio A.

(customize-set-variable 'crafted-startup-inhibit-splash t)
;; Revertimos esa configuración en que el buffer *scratch* está en
;; modo fundamental.
(customize-set-variable 'initial-major-mode 'lisp-interaction-mode)


;;; User's group for customize config

(defgroup crafted-config '()
  "User's configuration space for Crafted Emacs."
  :tag "Crafted Config"
  :group 'crafted)


;;; Core Settings

(require 'crafted-defaults)

(column-number-mode +1)


;;; User Interface

(require 'crafted-ui)

;; Install minions
(crafted-package-install-package 'minions)
(add-hook 'doom-modeline-mode-hook 'minions-mode)

(custom-set-variables
 '(crafted-ui-default-font '(:font "Noto Sans Mono" :height 120))
 '(crafted-ui-display-line-numbers t))

;; Load my preferred theme
;; TODO: replace with my own themes selection module.
;; (modus-themes-load-vivendi) ; already on early-config.el
(require 'config-modus)
(require 'config-themes)
(aba/theme-selection
 ;; (dark-variant  light-variant)
 '((modus-vivendi modus-operandi)
   (wombat dichromacy)
   (adwaita-dark adwaita)
   (tsdh-dark tsdh-light)
   (tango-dark tango)
   (doom-solarized-dark-high-contrast doom-solarized-light)
   ;; (doom-monokai-classic)
   ;; (doom-henna)
   ;; (doom-gruvbox doom-gruvbox-light)
   ))
(global-set-key (kbd "<f5>") #'aba/theme-selection-next)
(global-set-key (kbd "S-<f5>") #'aba/theme-selection-prev)
(global-set-key (kbd "C-<f5>") #'aba/theme-selection-toggle-variant)
(global-set-key (kbd "M-<f5>") #'aba/theme-selection-enable-current)


;;; Evil mode

(require 'crafted-evil)

;; Set configuration variables
(custom-set-variables '(crafted-evil-discourage-arrow-keys t))

;; Set preferred key bindings
(global-set-key (kbd "M-/") 'evilnc-comment-or-uncomment-lines)
(global-set-key (kbd "C-M-u") 'universal-argument)

(evil-global-set-key 'normal (kbd "C-t") 'transpose-chars) ;; to transpose things
(evil-global-set-key 'insert (kbd "C-t") 'transpose-chars) ;; to transpose things
(evil-global-set-key 'normal (kbd "M-t") 'transpose-words)
(evil-global-set-key 'insert (kbd "M-t") 'transpose-words)


;;; Custom leader key
(require 'config-leader-key)


;;; Completion and actions

(require 'crafted-completion)

(define-key vertico-map (kbd "C-f") 'vertico-exit)
(define-key minibuffer-local-map (kbd "C-d") 'embark-act)
(define-key project-prefix-map (kbd "g") 'consult-ripgrep)
(global-set-key (kbd "C-M-h") 'consult-buffer)


;;; Project Management

(require 'crafted-project)
(with-eval-after-load 'config-tab-bar
  (require 'config-project-tab))


;;; Source Control

(crafted-package-install-package 'magit)
(setq magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1)

(global-set-key (kbd "C-x g") 'magit-status)


;;; Org Mode


;;; Turn on variable-pitch for non-monospace fonts
(variable-pitch-mode +1)


;;; date + time
(require 'config-date-time)


;;; Tab-bar
(require 'config-tab-bar)


;;; Screencast

(when (ignore) ; HERE
  (require 'config-screencast))


;;; IDE

(require 'crafted-ide)

(add-hook 'c-mode 'eglot-ensure)

;; (crafted-package-install-package 'typescript-mode)


;;; Lisp Editing

(require 'crafted-lisp)


;;; Shells

(require 'config-eshell)
(require 'config-vterm)


;;; crafted-compile

;; (require 'crafted-compile)
;; (setq crafted-compile-user-configuration t
;;       crafted-compile-init-files nil
;;       crafted-compile-modules t
;;       crafted-compile-user-modules t
;;       crafted-compile-on-save t
;;       crafted-compile-module-list '(crafted-ui
;;                                      config-leader-key
;;                                      config-vterm))


;;; Windmove

(require 'crafted-windows)

(global-set-key (kbd "C-M-<left>") 'windmove-left)
(global-set-key (kbd "C-M-<up>") 'windmove-up)
(global-set-key (kbd "C-M-<right>") 'windmove-right)
(global-set-key (kbd "C-M-<down>") 'windmove-down)

(global-set-key (kbd "S-C-M-<left>") 'windmove-swap-states-left)
(global-set-key (kbd "S-C-M-<up>") 'windmove-swap-states-up)
(global-set-key (kbd "S-C-M-<right>") 'windmove-swap-states-right)
(global-set-key (kbd "S-C-M-<down>") 'windmove-swap-states-down)


;;; Administración de contraseñas (passwords)

(require 'config-password-store)


;;; Comunicación

;; Noticias && RSS (gnus)
(require 'config-gnus)
;; (require 'config-newsticker)

;; Correo (mu4e)
(define-key leader-map
  (kbd "C-m")
  (lambda () (interactive)
    (loader 'config-mu4e #'aba/mu4e)))
                  
;; IRC (erc)
(define-key leader-map
  (kbd "C-i")
  (lambda () (interactive)
    (loader 'config-erc #'aba/erc)))

;; Matrix (ement)
(define-key leader-map
  (kbd "C-e")
  (lambda () (interactive)
    (loader 'config-ement #'aba/ement)))

;; Telegram (telega)
(define-key leader-map
  (kbd "C-t")
  (lambda () (interactive)
    (loader 'config-telega #'aba/telega)))

(if (or (daemonp)
        (server-running-p))
    (message "Server or daemon is runnig!")
  (message "Starting emacs server.")
  (server-start))


;;; Prueba de paquetes
(require 'testing-obvious)
