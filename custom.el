(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(crafted-evil-discourage-arrow-keys nil)
 ;; '(crafted-ui-default-font '(:font "Noto Sans Mono" :height 120))
 '(crafted-ui-display-line-numbers t)
 '(display-time-day-and-date t)
 '(display-time-format "%a %d %b, %Y, %T")
 '(display-time-interval 1)
 '(ement-save-sessions t)
 '(enable-local-variables :all)
 '(ispell-dictionary "castellano")
 '(ispell-program-name "aspell")
 '(package-selected-packages
   '(project-tab-groups csv denote quelpa-use-package taxy plz ement mu4e cider sly-repl-ansi-color sly-quicklisp sly-asdf sly aggressive-indent xterm-color eglot minions non-existent/module non-existant/module evil-nerd-commenter evil-collection undo-tree evil treemacs yasnippet which-key vterm vertico telega password-store orderless marginalia magit keycast hl-sentence helpful embark-consult elisp-demos doom-themes doom-modeline corfu-doc company-web cape 0x0))
 '(package-user-dir
   (expand-file-name "rational-emacs/elpa" config-xdg-cache-home))
 '(teacher-email 'alex@ithilien)
 '(teacher-name "Profr. Alejandro Barocio Alvarez")
 '(teacher-schools ["Tec80"]))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
