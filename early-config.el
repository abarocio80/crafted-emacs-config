;; No carguemos el archivo "custom.el"
(setq crafted-load-custom-file nil)

;; Usemos stragiht.el
(setq crafted-package-system 'straight)
(crafted-package-bootstrap crafted-package-system)

;; Cambiamos el tema por defecto.
(disable-theme 'deeper-blue)
(load-theme 'modus-vivendi t)

;; Definimos una opacidad de 90%.
(defvar aba/frame-alpha 90
  "The value of the opacity to use in all frames.")

;; Creamos un comando para cambiar la opacidad.
(defun aba/--toggle-frame-alpha (alpha &optional set)
  (let ((a (if alpha
               (if (< alpha 50)
                   50
                 (if (> alpha 100)
                     100
                   (or alpha 100)))
             100)))
    (message "Changing frame alpha to: %s" (object-print a))
    (set-frame-parameter nil 'alpha `(,a . ,a))
    (when (and set alpha)
      (setq default-frame-alist (assq-delete-all 'alpha default-frame-alist))
      (add-to-list 'default-frame-alist `(alpha . (,a . ,a))))))
;; (aba/--toggle-frame-alpha nil)

(defun aba/toggle-frame-alpha (&optional p alpha)
  "Toggles the opacity of all the frames.

Togles betwen 100 and `aba/frame-alpha'.

 If the optional parameter ALPHA is non-nil, it will set
`aba/frame-alpha' to that value and change the opacity to that
value."
  (interactive "p")
  (message "%s" (object-print p))
  (if alpha
      (aba/--toggle-frame-alpha alpha t)
    (let* ((cur (car (frame-parameter nil 'alpha)))
           (a (if (and p (> p 1))
                  p alpha)))
      (if (or (and p (> p 1))
              (eq cur nil)
              (eql cur 100))
          (aba/--toggle-frame-alpha (or a aba/frame-alpha) (eql a aba/frame-alpha))
        (if (eql aba/frame-alpha cur)
            (aba/--toggle-frame-alpha 100 nil)
          (aba/--toggle-frame-alpha aba/frame-alpha nil)))))
  (cdr (assoc 'alpha default-frame-alist)))

;; Cambiamos la opacidad.
(aba/toggle-frame-alpha aba/frame-alpha)

;; Creamos un atajo de teclado para cambiar la opacidad.
(global-set-key (kbd "C-<f11>") #'aba/toggle-frame-alpha)

;; Definimos que cada ventana inicie maximizada.
(set-frame-parameter (selected-frame) 'fullscreen 'maximized)
(add-to-list 'default-frame-alist '(fullscreen . maximized))

;; Cargamos la configuración de variables de entorno.
;; (require 'config-env)
(load-file (expand-file-name "custom-modules/config-env.el" crafted-config-path))

(defmacro loader (lib func &optional warn)
  "When LIB could be loaded, execute FUNC."
  `(if (require ,lib)
       (funcall ,func)
     (if ,warn
         (warn "Could not load library '%s, nor execute function #'%s" ,lib ,func)
       (error "Could not load library '%s, nor execute function #'%s" ,lib ,func))))

(defun aba/kill-buffer (buffer)
  "Tries to kill BUFFER if it exists.

When BUFFER doesn't exist, it returns non-nil.  Otherwise, it
returns the evaluation of `kill-buffer' on BUFFER."
  (let ((b (get-buffer buffer)))
    (if b
        (kill-buffer b)
      t)))

(defun aba/goto-crafted-splash ()
  (if crafted-startup-inhibit-splash
      (switch-to-buffer "*Crafted Emacs*")
    (switch-to-buffer "*scratch*")))

(add-hook 'emacs-startup-hook 'aba/goto-crafted-splash)
(add-hook 'server-after-make-frame-hook 'aba/goto-crafted-splash)
