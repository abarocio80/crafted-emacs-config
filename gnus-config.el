;;; gnus -- Gnus configuration -*- mode:emacs-lisp -*-

;; Este es mi archivo de configuración de Gnus.

;;; User information
(setq user-full-name "Alejandro Barocio A."
      user-mail-address "abarocio80@gmail.com")

;;; News

;; https://www.emacswiki.org/emacs/Gmane
(setq gnus-select-method '(nntp "news.gmane.io"))

;;; Mail -- IMAP server
;; (add-to-list 'gnus-secondary-select-methods
;;              '(nnml "Personal"
;;                (nnml-directory "~/Mail/personal/"))
;;              '(nnml "Work"
;;                (nnml-directory "~/Mail/work/")))
             
;;; Mail -- maildir
;; (add-to-list 'gnus-secondary-select-methods
;;              '(nnmaildir "Personal"
;;                          (directory "~/Mail/personal/")))
;; (add-to-list 'gnus-secondary-select-methods
;;              '(nnmaildir "Work"
;;                          (directory "~/Mail/work/")))

;;; RSS
;; (add-to-list 'gnus-secondary-select-methods
             ;; '(nnrss ""))

;;; Groups
(setq gnus-group-line-format ;"%M%S%5y:%B(%g)\n")
      "%M%m%p%O%S %7t %7N %7U: %z [%7F] <%d> %-25,25G (%c) %30B\n"
      gnus-summary-line-format ;"%U%R%z%I%(%[%4L: %-23,23f%]%) %s\n"
      "%U%R%z%-30,30D %I %(%[%4L/%4k: %-23,23n%]%) %s\n")
